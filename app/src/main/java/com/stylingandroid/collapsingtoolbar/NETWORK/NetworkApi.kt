package com.interco.e.daggerrxretrofit.NETWORK

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Created by emine on 14/12/2018.
 */
interface NetworkApi {

    @Headers("Content-Type: application/json")
    @POST(BaseRoutes.TOKEN)
    fun getToken(@Body apiAuth: String): Single<String>



}
