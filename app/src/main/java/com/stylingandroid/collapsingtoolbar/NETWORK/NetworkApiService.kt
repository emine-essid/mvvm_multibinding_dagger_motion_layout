package com.stylingandroid.collapsingtoolbar.NETWORK

import com.google.gson.Gson
import com.interco.e.daggerrxretrofit.NETWORK.NetworkApi
import io.reactivex.Single
import retrofit2.Retrofit
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by emine on 14/12/2018.
 */
@Singleton
open class NetworkApiService @Inject
constructor(var gson: Gson, var retrofit: Retrofit) {

    private val networkApi: NetworkApi = retrofit.create(NetworkApi::class.java)

    fun requestToken(): Single<String> {
        return networkApi.getToken("hello")
    }

    fun networkInjectedSucsees(): String = " netwok injected ;)"


}
