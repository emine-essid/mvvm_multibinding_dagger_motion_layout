package com.stylingandroid.collapsingtoolbar.di.modules.dependencies.fragmentOne

import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.FragmentOneRepository
import dagger.Module
import dagger.Provides


@Module
class FragmentLandingOneDependencyModule {

    @Provides
    fun provideSplashepo(): FragmentOneRepository {
        return FragmentOneRepository()
    }


}