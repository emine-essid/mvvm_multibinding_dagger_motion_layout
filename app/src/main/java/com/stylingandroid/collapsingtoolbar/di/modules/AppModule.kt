package com.stylingandroid.collapsingtoolbar.di.modules

import android.app.Application
import android.content.Context
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.request.RequestOptions
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.stylingandroid.collapsingtoolbar.BuildConfig
import com.stylingandroid.collapsingtoolbar.R

import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


@Module
class AppModule {


    @Provides
    @Singleton
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun provideGson(): Gson {
        val gson = GsonBuilder()
                .setLenient()
                .create()
        return gson
    }


    @Singleton
    @Provides
    fun provideRequestOptions(): RequestOptions {
        return RequestOptions
                .placeholderOf(R.drawable.half_cercle_one)
                .error(R.drawable.half_cercle_three);
    }

    @Provides
    fun provideGlideInstance(application: Application, requestOptions: RequestOptions): RequestManager {
        return Glide.with(application)
                .setDefaultRequestOptions(requestOptions);
    }


//    ---------------


    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit {

        val requestInterceptor = { chain: Interceptor.Chain ->


            val url = chain.request()
                    .url()
                    .newBuilder()
                    //.addQueryParameter("key", API_KEY)
                    .build()

            val request = chain.request()
                    .newBuilder()
                    .url(url)
                    .build()

            Log.e("**", "-----------> " + request.toString())



            chain.proceed(request)
        }


        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(requestInterceptor)
//                .addNetworkInterceptor(StethoInterceptor())
//                .addInterceptor(ConnectivityInterceptor(context))
//                .addInterceptor(AuthenticationInterceptor())
                //.authenticator(TokenAuthenticator())
                .connectTimeout(6, TimeUnit.SECONDS)
                .readTimeout(6, TimeUnit.SECONDS)
                .build()


        return Retrofit.Builder().baseUrl("https://edge.blablacar.com/")
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
    }


}