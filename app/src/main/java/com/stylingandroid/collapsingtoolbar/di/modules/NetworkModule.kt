package com.stylingandroid.collapsingtoolbar.di.modules

import com.google.gson.Gson
import com.stylingandroid.collapsingtoolbar.NETWORK.NetworkApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class NetworkModule {

    @Provides
    fun provideApiService(gson: Gson, retrofit: Retrofit): NetworkApiService {
        return NetworkApiService(gson, retrofit)
    }

}