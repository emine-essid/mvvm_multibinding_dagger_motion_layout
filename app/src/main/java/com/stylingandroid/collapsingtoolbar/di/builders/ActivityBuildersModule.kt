package com.stylingandroid.collapsingtoolbar.di.builders

import com.stylingandroid.collapsingtoolbar.base.BaseActivity
import com.stylingandroid.collapsingtoolbar.di.modules.AppModule
import com.stylingandroid.collapsingtoolbar.di.modules.NetworkModule
import com.stylingandroid.collapsingtoolbar.di.modules.dependencies.fragmentOne.FragmentLandingOneDependencyModule
import com.stylingandroid.collapsingtoolbar.di.modules.dependencies.fragmentOne.FragmentLandingOneViewModelModule
import com.stylingandroid.collapsingtoolbar.di.modules.dependencies.main.MainDependencyModule
import com.stylingandroid.collapsingtoolbar.di.modules.dependencies.main.MainViewModelModule
import com.stylingandroid.collapsingtoolbar.ui.main.MainActivity
import com.stylingandroid.collapsingtoolbar.ui.splash.SplashActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeBaseActivity(): BaseActivity

    @ContributesAndroidInjector
    abstract fun contributeSplashActivity(): SplashActivity

    @ContributesAndroidInjector(modules = [
        NetworkModule::class,
        MainViewModelModule::class,
        MainDependencyModule::class])
    abstract fun contributeMainActivity(): MainActivity

}