package com.stylingandroid.collapsingtoolbar.di.componants

import android.app.Application
import com.stylingandroid.collapsingtoolbar.NETWORK.NetworkApiService
import com.stylingandroid.collapsingtoolbar.MApplication
import com.stylingandroid.collapsingtoolbar.di.builders.ActivityBuildersModule
import com.stylingandroid.collapsingtoolbar.di.modules.AppModule
import com.stylingandroid.collapsingtoolbar.di.builders.FragmentsBuilderModule
import com.stylingandroid.collapsingtoolbar.di.utils.ViewModelFactoryModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ActivityBuildersModule::class,
    FragmentsBuilderModule::class,
    AppModule::class,
    ViewModelFactoryModule::class
])
interface AppComponent : AndroidInjector<MApplication> {


    override fun inject(app: MApplication)

    fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }
}