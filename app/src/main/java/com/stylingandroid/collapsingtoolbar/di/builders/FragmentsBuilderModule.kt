package com.stylingandroid.collapsingtoolbar.di.builders

import com.stylingandroid.collapsingtoolbar.di.modules.dependencies.fragmentOne.FragmentLandingOneDependencyModule
import com.stylingandroid.collapsingtoolbar.di.modules.dependencies.fragmentOne.FragmentLandingOneViewModelModule
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.ViewModelFragmentLandingOne
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsBuilderModule {

   @ContributesAndroidInjector(modules = [
      FragmentLandingOneViewModelModule::class,
      FragmentLandingOneDependencyModule::class])
   abstract fun contributeMainFragment(): ViewModelFragmentLandingOne


}