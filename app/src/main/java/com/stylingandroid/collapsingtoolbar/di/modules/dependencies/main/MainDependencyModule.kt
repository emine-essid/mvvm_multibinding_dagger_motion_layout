package com.stylingandroid.collapsingtoolbar.di.modules.dependencies.main

import com.stylingandroid.collapsingtoolbar.NETWORK.NetworkApiService
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.FragmentOneRepository
import com.stylingandroid.collapsingtoolbar.ui.main.MainRepository
import dagger.Module
import dagger.Provides


@Module
class MainDependencyModule {


    @Provides
    fun provideSplashepo(networkApiService: NetworkApiService): MainRepository {
        return MainRepository(networkApiService)
    }


}