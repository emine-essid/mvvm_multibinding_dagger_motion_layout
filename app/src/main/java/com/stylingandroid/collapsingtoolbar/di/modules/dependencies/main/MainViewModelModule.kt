package com.stylingandroid.collapsingtoolbar.di.modules.dependencies.main

import androidx.lifecycle.ViewModel
import com.stylingandroid.collapsingtoolbar.di.keys.ViewModelKey
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.FragmentOneViewModel
import com.stylingandroid.collapsingtoolbar.ui.main.MainViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class MainViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun provideMainViewModelModule(myViewModel: MainViewModel): ViewModel


}