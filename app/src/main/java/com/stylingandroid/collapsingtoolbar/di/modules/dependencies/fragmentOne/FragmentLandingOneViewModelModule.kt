package com.stylingandroid.collapsingtoolbar.di.modules.dependencies.fragmentOne

import androidx.lifecycle.ViewModel
import com.stylingandroid.collapsingtoolbar.di.keys.ViewModelKey
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.FragmentOneViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class FragmentLandingOneViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(FragmentOneViewModel::class)
    abstract fun provideSplashViewModel(myViewModel: FragmentOneViewModel): ViewModel


}