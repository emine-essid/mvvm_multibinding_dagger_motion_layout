package com.stylingandroid.collapsingtoolbar.ui.splash

import android.os.Bundle
import androidx.fragment.app.FragmentManager
import com.stylingandroid.collapsingtoolbar.base.BaseActivity
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.FragmentLandingThree
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.FragmentLandingTwo
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.ViewModelFragmentLandingOne
import com.stylingandroid.collapsingtoolbar.utils.addFragment
import com.stylingandroid.collapsingtoolbar.utils.findLocationOfCenterOnTheScreen
import com.stylingandroid.collapsingtoolbar.utils.removeAllFragments
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(com.stylingandroid.collapsingtoolbar.R.layout.activity_splash)
        moveToNextFragment(1)
    }


    fun moveToNextFragment(k: Int) {
        when (k) {
            1 -> {
                supportFragmentManager.removeAllFragments()
                addFragment(ViewModelFragmentLandingOne.newInstance(view2.findLocationOfCenterOnTheScreen()), fragment_container.id)
            }
            2 -> addFragment(FragmentLandingTwo.newInstance(view2.findLocationOfCenterOnTheScreen()), fragment_container.id)
            3 -> addFragment(FragmentLandingThree(), fragment_container.id)
            else -> throw(Exception(" fuck wrong id!"))

        }

    }


}
