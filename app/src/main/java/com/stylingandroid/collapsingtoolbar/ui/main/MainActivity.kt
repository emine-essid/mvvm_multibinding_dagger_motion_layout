package com.stylingandroid.collapsingtoolbar.ui.main

import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.stylingandroid.collapsingtoolbar.R
import com.stylingandroid.collapsingtoolbar.base.BaseViewModelActivity
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.FragmentLandingThree
import com.stylingandroid.collapsingtoolbar.utils.addFragment
import kotlinx.android.synthetic.main.activity_main_test.*

class MainActivity : BaseViewModelActivity<MainViewModel>() {

    var toggleAnimation: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_test)

        (motion_dopDown as MotionLayout).transitionToEnd()


        // Creates a vertical Layout Manager
        recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.HORIZONTAL, false)


        // Access the RecyclerView Adapter and load the data into it
        recyclerView.adapter = AnimalAdapter(this)



        Log.e("***   ", "main Opned ;)")

        viewModel.testIsRepoWorking()

        viewModel.repoMessage.observe(this, Observer {
            Log.e("***>   ", it)

        })


        view4.setOnClickListener {

            Log.e("toggleAnimation", toggleAnimation.toString())

            if (!toggleAnimation) {
                (motion_dopDown as MotionLayout).transitionToStart()
                toggleAnimation = true
            } else {
                (motion_dopDown as MotionLayout).transitionToEnd()
                toggleAnimation = false
            }
        }


    }
}
