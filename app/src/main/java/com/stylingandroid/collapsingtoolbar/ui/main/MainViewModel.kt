package com.stylingandroid.collapsingtoolbar.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.FragmentOneRepository

import javax.inject.Inject

class MainViewModel @Inject
constructor(private val fragmentOneRepository: MainRepository) : ViewModel() {


    var repoMessage = MutableLiveData<String>()


    fun testIsRepoWorking() {
        repoMessage.postValue(fragmentOneRepository.sayhello())
    }
}
