package com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one;

import javax.inject.Inject;

public class FragmentOneRepository {
    @Inject
    public FragmentOneRepository() {
    }

    String sayhello() {

        return " **************** Hello from Fragment One Repository ***********************";
    }
}
