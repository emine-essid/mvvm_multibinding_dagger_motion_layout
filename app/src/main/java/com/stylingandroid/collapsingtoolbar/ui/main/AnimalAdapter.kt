package com.stylingandroid.collapsingtoolbar.ui.main

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.recycler_item.view.*
import java.util.*


class AnimalAdapter(val context: Context) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(com.stylingandroid.collapsingtoolbar.R.layout.recycler_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        val k = Color.argb(255, Random().nextInt(256), Random().nextInt(256), Random().nextInt(256))

        holder.moving_drawer.setBackgroundColor(k)
    }

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return 5
    }


}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val moving_drawer = view.moving_drawer

}


