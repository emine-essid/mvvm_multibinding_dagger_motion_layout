package com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.lifecycle.Observer

import com.stylingandroid.collapsingtoolbar.R
import com.stylingandroid.collapsingtoolbar.base.BaseViewModelFragment
import com.stylingandroid.collapsingtoolbar.base.BaseViewModelFragmentCircular
import com.stylingandroid.collapsingtoolbar.ui.splash.SplashActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_fragment_landing_one.*


class ViewModelFragmentLandingOne : BaseViewModelFragmentCircular<FragmentOneViewModel>() {
    override var posX: Int? = null

    override var posY: Int? = null


    override fun isToExitWithAnimation(): Boolean {
        return true
    }

    override fun provideView(): View {
        return this.view!!
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_landing_one, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onViewCreated(view, savedInstanceState)

        startCircularRevealVertical(true)
        viewModel.testIsRepoWorking()

        initObservers()

        (fist_fragment_container as MotionLayout).transitionToEnd()


        imageView4.setOnClickListener {
            (fist_fragment_container as MotionLayout).setTransition(R.id.test, R.id.testend);
            (fist_fragment_container as MotionLayout).transitionToEnd()
        }

        imageView.setOnClickListener {

            (activity as SplashActivity).moveToNextFragment(2)

        }
    }


    private fun initObservers() {
        viewModel.repoMessage.observe(this, Observer {

            Log.e("SplashTestViewModel", it)
        })

    }


    companion object {
        @JvmStatic
        fun newInstance(exit: IntArray? = null): ViewModelFragmentLandingOne = ViewModelFragmentLandingOne().apply {
            if (exit != null && exit.size == 2) {
                posX = exit[0]
                posY = exit[1]
            }
        }
    }

}
