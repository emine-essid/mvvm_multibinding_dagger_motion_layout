package com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial


import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout

import com.stylingandroid.collapsingtoolbar.R
import com.stylingandroid.collapsingtoolbar.base.BaseFragmentCircular
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.ViewModelFragmentLandingOne
import com.stylingandroid.collapsingtoolbar.ui.splash.SplashActivity
import kotlinx.android.synthetic.main.fragment_fragment_landing_two.*

class FragmentLandingTwo : BaseFragmentCircular() {
    override var posX: Int? = null
    override var posY: Int? = null

    override fun isToExitWithAnimation(): Boolean {
        return true
    }

    override fun provideView(): View {
        return view!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_landing_two, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startCircularRevealVertical(true)

        (secound_fragment_container as MotionLayout).transitionToEnd()
        imageView.setOnClickListener {
            //            (secound_fragment_container as MotionLayout).transitionToStart().also {
//                Handler().postDelayed({
//                }, 500)
//
//
//            }
            (activity as SplashActivity).moveToNextFragment(3)

        }
    }


    companion object {
        @JvmStatic
        fun newInstance(exit: IntArray? = null): FragmentLandingTwo = FragmentLandingTwo().apply {
            if (exit != null && exit.size == 2) {
                posX = exit[0]
                posY = exit[1]
            }
        }
    }

}
