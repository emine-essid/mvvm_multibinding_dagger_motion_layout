package com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.FragmentOneRepository

import javax.inject.Inject

class FragmentOneViewModel @Inject
constructor(private val fragmentOneRepository: FragmentOneRepository) : ViewModel() {


    var repoMessage = MutableLiveData<String>()


    fun testIsRepoWorking() {
        repoMessage.postValue(fragmentOneRepository.sayhello())
    }
}
