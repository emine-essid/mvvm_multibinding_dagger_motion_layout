package com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.Fragment
import com.stylingandroid.collapsingtoolbar.R
import com.stylingandroid.collapsingtoolbar.base.BaseFragmentCircular
import com.stylingandroid.collapsingtoolbar.ui.main.MainActivity
import com.stylingandroid.collapsingtoolbar.ui.splash.SplashActivity
import com.stylingandroid.collapsingtoolbar.utils.openActivity
import kotlinx.android.synthetic.main.fragment_fragment_landing_three.*

class FragmentLandingThree : BaseFragmentCircular() {
    override var posX: Int? = null
    override var posY: Int? = null

    override fun isToExitWithAnimation(): Boolean {
        return true
    }

    override fun provideView(): View {
        return view!!
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fragment_landing_three, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        startCircularRevealVertical(true)

        (therd_fragment_container as MotionLayout).transitionToEnd()
        imageView.setOnClickListener {
            //            if (activity is SplashActivity)
//                (activity as SplashActivity).openActivity(MainActivity::class.java, null, null)


            (activity as SplashActivity).openActivity(MainActivity::class.java, null, null)
        }


    }


    companion object {
        @JvmStatic
        fun newInstance(exit: IntArray? = null): FragmentLandingThree = FragmentLandingThree().apply {
            if (exit != null && exit.size == 2) {
                posX = exit[0]
                posY = exit[1]
            }
        }
    }

}
