package com.stylingandroid.collapsingtoolbar.base

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import android.util.AttributeSet
import android.util.Log
import android.view.*
import android.view.animation.DecelerateInterpolator
import androidx.annotation.MainThread
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.stylingandroid.collapsingtoolbar.R
import com.stylingandroid.collapsingtoolbar.di.utils.DaggerViewModelFactory
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.FragmentOneViewModel
import com.stylingandroid.collapsingtoolbar.ui.splash.SplashActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_fragment_landing_one.*
import java.lang.reflect.ParameterizedType
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class BaseFragmentCircular : Fragment() {

    abstract var posX: Int?
    abstract var posY: Int?

    abstract fun isToExitWithAnimation(): Boolean

    abstract fun provideView(): View


    /**
     * Starts circular reveal animation
     * [fromLeft] if `true` then start animation from the bottom left of the [View] else start from the bottom right
     */
    fun startCircularRevealHorizontal(fromLeft: Boolean) {
        provideView().addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int,
                                        oldRight: Int, oldBottom: Int) {
                v.removeOnLayoutChangeListener(this)
                val cx = if (fromLeft) v.left else v.right
                val cy = v.bottom
                val radius = Math.hypot(right.toDouble(), bottom.toDouble()).toInt()
                ViewAnimationUtils.createCircularReveal(v, cx, cy, 0f, radius.toFloat()).apply {
                    interpolator = DecelerateInterpolator(2f)
                    duration = 1000
                    start()
                }
            }
        })
    }

    /**
     * Starts circular reveal animation
     * [fromButtom] if `true` then start animation from the bottom Center of the [View] else start from the bottom right
     */

    fun startCircularRevealVertical(fromButtom: Boolean) {
        provideView().addOnLayoutChangeListener(object : View.OnLayoutChangeListener {
            override fun onLayoutChange(v: View, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int,
                                        oldRight: Int, oldBottom: Int) {
                v.removeOnLayoutChangeListener(this)
                val cx = v.width / 2
                val cy = if (fromButtom) v.bottom else v.top
                val radius = Math.hypot(right.toDouble(), bottom.toDouble()).toInt()
                ViewAnimationUtils.createCircularReveal(v, cx, cy, 0f, radius.toFloat()).apply {
                    interpolator = DecelerateInterpolator(2f)
                    duration = 1000
                    start()
                }
            }
        })
    }

    /**
     * Animate fragment exit using given parameters as animation end point. Runs the given block of code
     * after animation completion.
     *
     * @param exitX: Animation end point X coordinate.
     * @param exitY: Animation end point Y coordinate.
     * @param block: Block of code to be executed on animation completion.
     */
    fun exitCircularReveal(exitX: Int, exitY: Int, block: () -> Unit) {
        val startRadius = Math.hypot(provideView().width.toDouble(), provideView().height.toDouble())
        ViewAnimationUtils.createCircularReveal(provideView(), exitX, exitY, startRadius.toFloat(), 0f).apply {
            duration = 350
            interpolator = DecelerateInterpolator(1f)
            addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator?) {
                    block()
                    super.onAnimationEnd(animation)
                }
            })
            start()
        }
    }

    /**
     * @return the position of the current [View]'s center in the screen
     */


/*
    companion object {
        @JvmStatic
        fun newInstance(exit: IntArray? = null): OneFragment = OneFragment().apply {
            if (exit != null && exit.size == 2) {
                posX = exit[0]
                posY = exit[1]
            }
        }
    }
*/

}