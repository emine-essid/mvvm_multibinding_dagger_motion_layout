package com.stylingandroid.collapsingtoolbar.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.stylingandroid.collapsingtoolbar.di.utils.DaggerViewModelFactory
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import java.lang.reflect.ParameterizedType
import javax.inject.Inject


abstract class BaseViewModelActivity<V : ViewModel> : AppCompatActivity(), HasSupportFragmentInjector {


    @Inject
    lateinit var viewModelProviderFactory: DaggerViewModelFactory

    lateinit var viewModel: V

    //IMPORTNT INJECTOR DISPATCHER FOR FRAGMENTS
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        //IMPORTNT INJECTOR
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, (viewModelProviderFactory as ViewModelProvider.Factory)).get(viewModelClass())

    }


    //YOU KNOW IT
    override fun supportFragmentInjector() = dispatchingAndroidInjector


    @Suppress("UNCHECKED_CAST")
    private fun viewModelClass(): Class<V> {
        // dirty hack to get generic type https://stackoverflow.com/a/1901275/719212
        return ((javaClass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<V>)
    }
}
