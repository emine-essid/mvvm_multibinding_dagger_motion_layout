package com.stylingandroid.collapsingtoolbar.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import dagger.android.AndroidInjection
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity(), HasSupportFragmentInjector {

    //IMPORTNT INJECTOR DISPATCHER FOR FRAGMENTS
    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun onCreate(savedInstanceState: Bundle?) {
        //IMPORTNT INJECTOR
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }


    //YOU KNOW IT
    override fun supportFragmentInjector() = dispatchingAndroidInjector
}
