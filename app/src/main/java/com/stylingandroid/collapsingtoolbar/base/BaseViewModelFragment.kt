package com.stylingandroid.collapsingtoolbar.base

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.MainThread
import androidx.constraintlayout.motion.widget.MotionLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.stylingandroid.collapsingtoolbar.R
import com.stylingandroid.collapsingtoolbar.di.utils.DaggerViewModelFactory
import com.stylingandroid.collapsingtoolbar.ui.fragments_tutorial.one.FragmentOneViewModel
import com.stylingandroid.collapsingtoolbar.ui.splash.SplashActivity
import dagger.android.support.AndroidSupportInjection
import kotlinx.android.synthetic.main.fragment_fragment_landing_one.*
import java.lang.reflect.ParameterizedType
import javax.inject.Inject
import kotlin.reflect.KClass

abstract class BaseViewModelFragment<V : ViewModel> : Fragment() {


    @Inject
    lateinit var viewModelProviderFactory: DaggerViewModelFactory

    lateinit var viewModel: V

    abstract override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                                       savedInstanceState: Bundle?): View?


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        AndroidSupportInjection.inject(this)
        super.onViewCreated(view, savedInstanceState)


        viewModel = ViewModelProviders.of(this, (viewModelProviderFactory as ViewModelProvider.Factory)).get(viewModelClass())


    }


    @Suppress("UNCHECKED_CAST")
    private fun viewModelClass(): Class<V> {
        // dirty hack to get generic type https://stackoverflow.com/a/1901275/719212
        return ((javaClass.genericSuperclass as ParameterizedType)
                .actualTypeArguments[0] as Class<V>)
    }


}