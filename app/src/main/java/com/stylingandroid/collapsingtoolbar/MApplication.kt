package com.stylingandroid.collapsingtoolbar

import com.stylingandroid.collapsingtoolbar.di.componants.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication


class MApplication : DaggerApplication() {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent

    }


}
