package com.stylingandroid.collapsingtoolbar.utils

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils.replace
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.OneShotPreDrawListener.add
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

fun AppCompatActivity.addFragment(fragment: Fragment, frameId: Int) {
    supportFragmentManager.inTransaction {
        add(frameId, fragment)
        addToBackStack(fragment.javaClass.toString()
        )
    }
}


fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
    supportFragmentManager.inTransaction { replace(frameId, fragment) }
}


inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

fun <T> Context.openActivity(it: Class<T>, bundleKey: String?, bundle: Bundle?) {
    val intent = Intent(this, it)
    if (bundle != null && !bundleKey.isNullOrEmpty())
        intent.putExtra(bundleKey, bundle)
    startActivity(intent)
}


fun View.findLocationOfCenterOnTheScreen(): IntArray {
    val positions = intArrayOf(0, 0)
    this.getLocationInWindow(positions)
    // Get the center of the view
    positions[0] = positions[0] + this.width / 2
    positions[1] = positions[1] + this.height / 2
    return positions
}


fun FragmentManager.removeAllFragments() {
    this.fragments.forEach { fragment ->
        this.beginTransaction().remove(fragment).commit()
    }
}